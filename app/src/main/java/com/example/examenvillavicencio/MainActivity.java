package com.example.examenvillavicencio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private Button btnSalir;
    private Button btnEntrar;

    private EditText txtUsuario;
    private  String tituloMainActivity = "Cálculo de pago de nomina";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSalir = findViewById(R.id.btnSalir);
        btnEntrar = findViewById(R.id.btnEntrar);
        txtUsuario = findViewById(R.id.txtNombre);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String usuario = txtUsuario.getText().toString();
                Intent intent = new Intent(MainActivity.this, ReciboNomina.class);

                intent.putExtra("USUARIO", usuario);
                intent.putExtra("TITULO", tituloMainActivity);
                startActivity(intent);
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}