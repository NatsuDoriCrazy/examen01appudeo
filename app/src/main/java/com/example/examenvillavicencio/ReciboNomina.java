package com.example.examenvillavicencio;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ReciboNomina extends AppCompatActivity {
    private TextView lblTitulo2;
    private TextView txtNombrede2;

    private EditText usuario;

    private Button btnLimpiar;
    private Button btnRegresar;
    private Button btnCalcular;

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private RadioButton radioButton2;
    private RadioButton radioButton3;
    private EditText txtHoras;
    private EditText txtHorasExtras;
    private TextView subtotalTextView;
    private TextView impuestoTextView;
    private TextView totalAPagarTextView;
    private EditText editTextText;
    private EditText editTextText2;
    double pagoPorHorasNormalesTrabajadas;
    double horasTrabajadas;
    double pagoHoraExtra;
    double horasExtras;
    double subtotal;
    double impuesto;
    double totalAPagar;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);

        lblTitulo2 = findViewById(R.id.lblTitulo2);
        txtNombrede2 = findViewById(R.id.txtNombreDe2);
        usuario = findViewById(R.id.txtNombre);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        radioGroup = findViewById(R.id.radioGroup);
        txtHoras = findViewById(R.id.txtHoras);
        txtHorasExtras = findViewById(R.id.txtHorasExtra);
        subtotalTextView = findViewById(R.id.subtotalTextView);
        impuestoTextView = findViewById(R.id.impuestoTextView);
        totalAPagarTextView = findViewById(R.id.totalAPagarTextView);
        editTextText = findViewById(R.id.editTextText);
        editTextText2 = findViewById(R.id.editTextText2);
        Intent intent = getIntent();
        String usuario = intent.getStringExtra("USUARIO");
        String tituloMainActivity = intent.getStringExtra("TITULO");


       lblTitulo2.setText(tituloMainActivity);
       txtNombrede2.setText(usuario);

       btnRegresar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });

       btnLimpiar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               txtHoras.setText("");
               txtHorasExtras.setText("");
               subtotalTextView.setText("");
               impuestoTextView.setText("");
               totalAPagarTextView.setText("");
               editTextText.setText("");
               editTextText2.setText("");

           }
       });

      btnCalcular.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
              double horas = Double.parseDouble(txtHoras.getText().toString());
              double horasext = Double.parseDouble(txtHorasExtras.getText().toString());
              double subtotal = 0.0;
              double impuesto = 0.0;
              double totalAPagar = 0.0;

              if (selectedRadioButtonId == R.id.radioButton3){
                  subtotal = horas * 100 + horasext * 200;
                  impuesto = subtotal * 0.16;
                  totalAPagar = subtotal - impuesto;
                  subtotalTextView.setText("" + subtotal);
                  impuestoTextView.setText(" " + impuesto);
                  totalAPagarTextView.setText("" + totalAPagar);

              }

              if (selectedRadioButtonId == R.id.radioButton2) {
                  subtotal = horas * 70 + horasext * 140;
                  impuesto = subtotal * 0.16;
                  totalAPagar = subtotal - impuesto;
                  subtotalTextView.setText("" + subtotal);
                  impuestoTextView.setText(" " + impuesto);
                  totalAPagarTextView.setText("" + totalAPagar);
              }
              if (selectedRadioButtonId == R.id.radioButton){
                  subtotal = horas * 50 + horasext * 100;
                  impuesto = subtotal * 0.16;
                  totalAPagar = subtotal - impuesto;
                  subtotalTextView.setText("" + subtotal);
                  impuestoTextView.setText(" " + impuesto);
                  totalAPagarTextView.setText("" + totalAPagar);
              }
          }
      });


       }
    }


